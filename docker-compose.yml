# Docker-compose configuration for development

version: '3.8'

x-common-spec: &common-spec
  env_file: .env

x-django-container-spec: &django-container-spec
  <<: *common-spec
  build:
    context: .
    dockerfile: Dockerfile-django
    args:
      DPT_VENV_CACHING: $DPT_VENV_CACHING
  environment:
    PYTHONUNBUFFERED: "0"
  volumes:
    - &py-code-volume "./marysia_spa:/app"
# -
    - ".data/media:/files/media"
    - &poetry-volume "${DPT_POETRY_CACHE_DIR:-.data/pycache/pypoetry}:/root/.cache/pypoetry"
    - &pip-volume "${DPT_PIP_CACHE_DIR:-.data/pycache/pip}:/root/.cache/pip"
  depends_on:
    - postgres
    - redis
    - mailhog

x-celery-environment: &celery-environment
  environment:
    PYTHONUNBUFFERED: "0"
    SKIP_INSTALL: "1"


services:
  django:
    <<: *django-container-spec
    ports:
      - "${DJANGO_PORT-8000}:80"
    # Add -Wall option to see the (deprecation) warnings
    # command: python -Wall manage.py runserver 0.0.0.0:80
    command: python manage.py runserver 0.0.0.0:80
    entrypoint: /usr/bin/wait-for-it.sh postgres:5432 -t 60 -- entrypoint.sh --

  node:
    <<: *common-spec
    build:
      context: .
      dockerfile: Dockerfile-node
# -
    ports:
# -
# -
      - "8000:8000"
      - "8001:8001"
# -
# -
    volumes:
# -
      - "./app:/app"
# -   
      - ".data/node_modules:/app/node_modules"
      - ".data/yarn:/usr/local/share/.cache/yarn"


# -
  celery:
    <<: *django-container-spec
    <<: *celery-environment
    restart: unless-stopped
    entrypoint: /usr/bin/wait-for-it.sh postgres:5432 -t 60 -- entrypoint.sh --
    command: celery --app marysia_spa worker --autoscale 6,2 --loglevel INFO

  celery_beat:
    <<: *django-container-spec
    <<: *celery-environment
    container_name: marysia_spa_celery_beat
    volumes:
      - ".data/celery:/celery"
      - *py-code-volume
      - *poetry-volume
      - *pip-volume
    depends_on:
      - redis
    entrypoint: entrypoint.sh --
    # Disable pidfile by specifying an empty one. We used fixed container_name which provides single-running-process
    #  guarantee and the lack of pidfile ensures that Celery Beat starts even if the Docker container was killed and
    #  then restarted (in which case the pidfile would still be present).
    command: celery --app marysia_spa beat --loglevel INFO --pidfile= --schedule /celery/celerybeat-schedule
# -

  postgres:
    image: postgres:12
    # Comment in the following lines to connect to your Dockerized instance of Postgres from your host machine.
    # Change the host port (before colon) if you have a local instance of Postgres running on that port.
    # ports:
    #     - "5432:5432"
    volumes:
      - ".data/postgres:/var/lib/postgresql/data"
      - ".data/db-mirror:/db-mirror"  # Used by ansible mirror playbook
      - "./scripts/create-citext-extension.sql:/docker-entrypoint-initdb.d/create-citext-extension.sql"
    environment:
      # credentials taken from .env file
      POSTGRES_USER: "${DJANGO_DATABASE_USER-'marysia_spa'}"
      POSTGRES_PASSWORD: "${DJANGO_DATABASE_PASSWORD-'marysia_spa'}"

  redis:
    image: redis:4.0.10-alpine
    sysctls:
      # To allow maintaining TCP backlog setting that defaults to 511
      net.core.somaxconn: 512
    volumes:
      - ".data/redis:/data"

  mailhog:
    image: mailhog/mailhog:v1.0.0
    ports:
      - "${MAILHOG_PORT-8025}:8025"
    logging:
      driver: "none"
