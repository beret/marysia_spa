export const URLS = {
    admin: '/adminpanel/',
    adminLogout: '/adminpanel/logout/',
    // - 
    // -     
    // -     
    // - 
    login: '/auth/login',
    logout: '/auth/logout',
};

export const adminAccount = {
    email: 'cypress-admin@localhost.localdomain',
    password: 'admin',
};

// to begin with, it's enough to use the same account as the admin account.
export const testAccount = {
    email: 'cypress-admin@localhost.localdomain',
    password: 'admin',
};
