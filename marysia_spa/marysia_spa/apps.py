from django.apps import AppConfig
from django.core import checks

from tg_utils.checks import check_production_settings, check_sentry_config


class MarysiaSpaConfig(AppConfig):
    name = "marysia_spa"
    verbose_name = "Marysia SPA"

    def ready(self):
        # Import and register the system checks
        checks.register(check_production_settings)
        checks.register(check_sentry_config)
