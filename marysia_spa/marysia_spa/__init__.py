# -
from marysia_spa.celery import app as celery_app


# -

default_app_config = "marysia_spa.apps.MarysiaSpaConfig"

# -

__all__ = ["celery_app"]
# -
